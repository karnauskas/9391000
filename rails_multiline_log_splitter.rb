require 'rubygems'
require 'yajl/json_gem'

# Simple flush to json, can add timestamp by parsing it.
def flush(buffer)
  puts buffer.to_json
end

# Initialize 
buffer = []
previous_line_matched = false
pattern_matched_count = 0 
current_pattern = 0
split_buffer = []
buffer_last_seen_at = 0


# List of patterns to match
# here we have two consecutive lines with only the newline
patterns = [
  /^\n$/,
  /^\n$/
]

STDIN.each_line do |line|

  # Perform match and increment.
  if matched = (line =~ patterns[current_pattern]) 
    pattern_matched_count += 1 
    current_pattern += 1
  end

  # Flush out if you reached patterns.length or the EOF
  if pattern_matched_count == patterns.length || STDIN.eof
    buffer << line.strip if STDIN.eof # add the last line if it's EOF.
    flush(buffer)
    # Reset 
    previous_line_matched = false
    pattern_matched_count = 0 
    current_pattern = 0
    split_buffer = []
    buffer_last_seen_at = 0
    buffer = []
    next
  elsif matched
    # we need to keep track of where we were when we found the first
    # match.  This is b/c we can get through a bunch of matches but
    # then fail on the last pattern.  We need to be able to keep track
    # so we can inject the patterns matched (wich are part of the log entry)
    # back in
    buffer_last_seen_at = buffer.length if previous_line_matched == false
    split_buffer << line
    previous_line_matched = true
  else
    # We matched a few already, but then didn't get to the finish line
    # we need to add the matched lines back in b/c they weren't really 
    # part of a split
    if pattern_matched_count > 0
      buffer.insert(buffer_last_seen_at, *split_buffer)
    end
    
    # We didn't match earlier, and we're not flushing
    # so we should reset b/c there's no consecutive matches
    # Reset 
    previous_line_matched = false
    pattern_matched_count = 0 
    current_pattern = 0
    split_buffer = []
    buffer_last_seen_at = 0
    
    # buffer up the log entry clean up newlines
    buffer << line.strip
  end
  
end
